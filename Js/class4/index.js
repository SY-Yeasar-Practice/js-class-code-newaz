//1.Arithmetic operator  (done)
//2. Comparison operator 
//3.logical operator
//4.type of operator
//5.Assignment  Operator

//today's topic 
//type of operator (done)
//  Comparison operator 
// variable declaration


// type of operator
// var a = 5
// console.log(typeof a);


//  Comparison operator 
// == , === , != , !== , < , > <= >= 
//output will always boolean (true or false)
//true = 1 false = 0

// == convert each data type into number then value check 
// === check data type and value
// var a = 1 // 1
// var b = "1" // 1
// console.log(a == b);
// console.log(a === b);

// var a = "5",b = 5
// // console.log(a != b);
// // !== convert into number and value check
// // //!  !== data type or value
// console.log(a !== b);

// < > 
// var a = 5
// var b = 10
// console.log(a < b);
// console.log(a > b);

// <= >=
// var a = 15
// var b = 15
// console.log(a <= b);
// console.log(a >= b);

//variable declaration

//type 1
// var a = 5
// var b = 10

//type 2
// var a = 5 , b = 6 
// console.log(a);
// console.log(b);

//type 3 
// var a , b 
// a = 15
// b = 10
// console.log(a);
// console.log(b);

//type 4
// var [a,b] = [10,15]
// console.log(a);
// console.log(b);


 





